import { VError } from 'verror';

import { createLogger, EventSeverity } from '../src';

const logger = createLogger({
  name: 'Name of the logger',
  filename: __filename,
  severity: EventSeverity.Debug,
});

logger.info('Youhou');
logger.debug('Test de debug');
logger.trace('Test de trace');
logger.warn('Test de warn');
logger.error('Test error', new Error('Test error'));

const err1 = new VError('something bad happened');

const err2 = new VError(
  {
    name: 'ConnectionError',
    cause: err1,
    info: {
      errno: 'ECONNREFUSED',
      remoteIp: '127.0.0.1',
      port: 215,
    },
  },
  'failed to connect to "127.0.0.1215"'
);

const err3 = new VError(
  {
    name: 'RequestError',
    cause: err2,
    info: {
      errno: 'EBADREQUEST',
    },
  },
  'request failed'
);

logger.error('A VError', err3);

const subLogger = logger.createSubLogger({
  ecsDocument: {
    trace: {
      id: 'youhou',
    },
  },
});

subLogger.info('Example');

logger.config.ecsDocument.transaction = {
  id: 'erwwerwer',
};

subLogger.info('Example 2');
