export enum EventSeverity {
  Trace = 0,
  Debug = 1,
  Info = 2,
  Notice = 3,
  Warning = 4,
  Error = 5,
  Critical = 6,
  Alert = 7,
  Emergency = 8,
}

/**
 * Convert event severity name to event severity
 */
export function getEventSeverityFromName(
  value: string | undefined,
  defaultValue: EventSeverity = EventSeverity.Info
): EventSeverity {
  if (value == null) {
    return defaultValue;
  }

  const eventSeverityName = (value.charAt(0).toUpperCase() +
    value.slice(1).toLowerCase()) as keyof typeof EventSeverity;
  const eventSerity = EventSeverity[eventSeverityName];
  if (eventSerity == null) {
    return defaultValue;
  }

  return eventSerity;
}
