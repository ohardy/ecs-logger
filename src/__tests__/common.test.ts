import { colorizeDuration } from '../common';

describe('common', (): void => {
  describe('#colorizeDuration', (): void => {
    it('should exist', (): void => {
      expect(typeof colorizeDuration).toEqual('function');
    });
  });
});
