import { createLogger, Logger } from '../logger';

describe('logger', (): void => {
  describe('#createLogger', (): void => {
    it('should exist', (): void => {
      expect(typeof createLogger).toEqual('function');
    });
  });

  describe('#Logger', (): void => {
    it('should exist', (): void => {
      expect(typeof Logger).toEqual('function');
    });
  });
});
