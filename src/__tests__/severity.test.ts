import { getEventSeverityFromName, EventSeverity } from '../severity';

describe('severity', (): void => {
  describe('#getEventSeverityFromName', (): void => {
    it('should exist', (): void => {
      expect(typeof getEventSeverityFromName).toEqual('function');
    });

    it('should return default severity', (): void => {
      // @ts-ignore
      const severity = getEventSeverityFromName();

      expect(severity).toEqual(EventSeverity.Info);
    });

    it('should return severity from name', (): void => {
      const severity = getEventSeverityFromName('info');

      expect(severity).toEqual(EventSeverity.Info);
    });

    it('should return default severity on invalid name', (): void => {
      const severity = getEventSeverityFromName('invalid');

      expect(severity).toEqual(EventSeverity.Info);
    });
  });
});
