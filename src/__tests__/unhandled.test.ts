import { handleUnhandledRejection } from '../unhandled';

describe('unhandled', (): void => {
  describe('#handleUnhandledRejection', (): void => {
    it('should exist', (): void => {
      expect(typeof handleUnhandledRejection).toEqual('function');
    });
  });
});
