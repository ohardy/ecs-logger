import chalk from 'chalk';
import { EventSeverity } from './severity';

export const EVENT_MODULE_COLOR = chalk.bgKeyword('cyan').keyword('black');

export const LINE_ELEMENTS = [
  '@timestamp',
  'event.severity',
  'event.module',
  'event.duration',
  'message',
  'error',
];

export const EVENT_DURATIONS = [
  {
    minDuration: 200,
    colorize: chalk.bgKeyword('red'),
  },
  {
    minDuration: 100,
    colorize: chalk.bgKeyword('yellow').keyword('black'),
  },
  {
    minDuration: 0,
    colorize: chalk.bgKeyword('lightgreen').keyword('black'),
  },
];

export const EVENT_SEVERITY_COLORS: { [key in EventSeverity]: Function } = {
  [EventSeverity.Alert]: chalk.bgKeyword('black'),
  [EventSeverity.Critical]: chalk.bgKeyword('black'),
  [EventSeverity.Debug]: chalk.bgKeyword('magenta').keyword('black'),
  [EventSeverity.Emergency]: chalk.bgKeyword('black'),
  [EventSeverity.Error]: chalk.bgKeyword('red'),
  [EventSeverity.Info]: chalk.bgKeyword('lightgreen').keyword('black'),
  [EventSeverity.Notice]: chalk.bgKeyword('black'),
  [EventSeverity.Trace]: chalk.bgKeyword('purple'),
  [EventSeverity.Warning]: chalk.bgKeyword('orange').keyword('black'),
};

export const EVENT_SEVERITY_NAMES: { [key in EventSeverity]: string } = {
  [EventSeverity.Alert]: 'ALERT',
  [EventSeverity.Critical]: 'CRITI',
  [EventSeverity.Debug]: 'DEBUG',
  [EventSeverity.Emergency]: 'EMERG',
  [EventSeverity.Error]: 'ERROR',
  [EventSeverity.Info]: 'INFO',
  [EventSeverity.Notice]: 'NOTICE',
  [EventSeverity.Trace]: 'TRACE',
  [EventSeverity.Warning]: 'WARN',
};
