import { ElasticKeyword } from './common';

export interface ServiceFields {
  ephemeralId?: ElasticKeyword; // ephemeral_id
  id?: ElasticKeyword;
  name?: ElasticKeyword;
  node?: {
    name?: ElasticKeyword;
  };
  state?: ElasticKeyword;
  type?: ElasticKeyword;
  version?: ElasticKeyword;
}
