import { ElasticKeyword } from './common';

export interface RuleFields {
  author?: ElasticKeyword;
  category?: ElasticKeyword;
  description?: ElasticKeyword;
  id?: ElasticKeyword;
  license?: ElasticKeyword;
  name?: ElasticKeyword;
  reference?: ElasticKeyword;
  ruleset?: ElasticKeyword;
  uuid?: ElasticKeyword;
  version?: ElasticKeyword;
}
