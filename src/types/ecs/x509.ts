import { ElasticKeyword } from './common';

export type X509FieldsIssuer = {
  commonName?: ElasticKeyword;
  country?: ElasticKeyword;
  distinguishedName?: ElasticKeyword;
  locality?: ElasticKeyword;
  organization?: ElasticKeyword;
  organizationalUnit?: ElasticKeyword;
  stateOrProvince?: ElasticKeyword;
};

export interface X509Fields {
  alternativeNames?: ElasticKeyword[];
  issuer?: X509FieldsIssuer;
  notAfter?: Date;
  notBefore?: Date;
  publicKeyAlgorithm?: ElasticKeyword;
  publicKeyCurve?: ElasticKeyword;
  publicKeyExponent?: number;
  publicKeySize?: number;
  serialNumber?: ElasticKeyword;
  signatureAlgorithm?: ElasticKeyword;
  subject?: X509FieldsIssuer;
  version_number?: ElasticKeyword;
}
