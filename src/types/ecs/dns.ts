import { IpAddress, ElasticKeyword } from './common';

export type DnsHeaderFlag = 'AA' | 'TC' | 'RD' | 'RA' | 'AD' | 'CD' | 'DO';

export interface DnsAnswer {
  class?: ElasticKeyword;
  data: ElasticKeyword;
  name?: ElasticKeyword;
  ttl?: number;
  type?: ElasticKeyword;
}

export interface DnsQuestion {
  class?: ElasticKeyword;
  name?: ElasticKeyword;
  registeredDomain?: ElasticKeyword; // registered_domain
  subdomain?: ElasticKeyword;
  topLevelDomain?: ElasticKeyword; // top_level_domain
  type?: ElasticKeyword;
}

export interface DnsAnswerFields {
  answers?: DnsAnswer[];
  headerFlags?: DnsHeaderFlag[]; // header_flags
  id?: ElasticKeyword;
  opCode?: ElasticKeyword; // op_code
  question?: DnsQuestion;
  resolvedIp?: IpAddress[]; // resolved_ip
  responseCode?: ElasticKeyword; // response_code
  type?: ElasticKeyword;
}

export interface DNSFields {
  answers?: DnsAnswerFields[];
}
