import { ElasticKeyword } from './common';

/**
 * An autonomous system (AS) is a collection of connected Internet Protocol
 * (IP) routing prefixes under the control of one or more network operators
 * on behalf of a single administrative entity or domain that presents a
 * common, clearly defined routing policy to the internet.
 *
 * The **as** fields are expected to be nested at:
 * - client.as
 * - destination.as
 * - server.as
 * - source.as.
 *
 * Note also that the as fields are not expected to be used directly at the top level.
 */
export interface AutonomousSystemFields {
  /**
   * Unique number allocated to the autonomous system. The autonomous system
   * number (ASN) uniquely identifies each network on the Internet.
   *
   * type: long
   *
   * level: extended
   *
   * @exampl: 15169
   */
  number?: number;
  organization?: {
    /**
     * Organization name.
     *
     * type: keyword
     *
     * level: extended
     *
     * @example Google LLC
     */
    name: ElasticKeyword;
  };
}
