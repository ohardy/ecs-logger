export type EcsVersion = '1.6.0';

export interface ECSFields {
  version: EcsVersion;
}
