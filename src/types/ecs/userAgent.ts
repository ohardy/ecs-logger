import { OperatingSystemFields } from './os';
import { ElasticKeyword } from './common';

export interface UserAgentFields {
  os?: OperatingSystemFields;

  device?: {
    name?: ElasticKeyword;
  };

  name?: ElasticKeyword;
  original?: ElasticKeyword;
  version?: ElasticKeyword;
}
