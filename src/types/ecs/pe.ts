import { ElasticKeyword } from './common';

export interface PeHeaderFields {
  architecture?: ElasticKeyword;
  company?: ElasticKeyword;
  description?: ElasticKeyword;
  fileVersion?: ElasticKeyword; // file_version
  imphash?: ElasticKeyword;
  originalFileName?: ElasticKeyword; // original_file_name
  product?: ElasticKeyword;
}
