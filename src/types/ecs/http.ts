// import { IncomingHttpHeaders } from 'http';

import { ElasticKeyword, ElasticText } from './common';

export interface HttpRequestFields {
  body?: {
    bytes?: number;
    content?: ElasticKeyword;
    'content.text'?: ElasticText;
  };
  bytes?: number;
  // graphql?: {
  //   operationName?: string;
  // };
  // headers?: IncomingHttpHeaders; // Doesn't exists in ECS
  method?: ElasticKeyword;
  referrer?: ElasticKeyword;
}

export interface HttpResponseFields {
  body?: {
    bytes?: number;
    content?: ElasticKeyword;
    'content.text'?: ElasticText;
  };
  bytes?: number;
  statusCode?: number; // status_code
}

export interface HttpFields {
  request?: HttpRequestFields;
  response?: HttpResponseFields;
  version?: ElasticKeyword;
}
