import { ElasticKeyword, ElasticText } from './common';

export interface OperatingSystemFields {
  family?: ElasticKeyword;
  full?: ElasticKeyword;
  kernel?: ElasticKeyword;
  name?: ElasticKeyword;
  'name.text'?: ElasticText;
  platform?: ElasticKeyword;
  version?: ElasticKeyword;
}
