import { ElasticKeyword } from './common';

export interface PackageFields {
  architecture?: ElasticKeyword;
  buildVersion?: ElasticKeyword; // build_version
  checksum?: ElasticKeyword;
  description?: ElasticKeyword;
  installScope?: ElasticKeyword; // install_scope
  installed?: Date;
  license?: ElasticKeyword;
  name?: ElasticKeyword;
  path?: ElasticKeyword;
  reference?: ElasticKeyword;
  size?: number;
  type?: ElasticKeyword;
  version?: ElasticKeyword;
}
