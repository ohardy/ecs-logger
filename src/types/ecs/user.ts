import { GroupFields } from './group';
import { ElasticKeyword, ElasticText } from './common';

export interface UserFields {
  group?: GroupFields;

  domain?: ElasticKeyword;
  email?: ElasticKeyword;
  fullName?: ElasticKeyword; // full_name
  'fullName.text'?: ElasticText;
  hash?: ElasticKeyword;
  id?: ElasticKeyword;
  name?: ElasticKeyword;
  'name.text'?: ElasticText;
  roles?: ElasticKeyword[];
}
