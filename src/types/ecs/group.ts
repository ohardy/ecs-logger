import { ElasticKeyword } from './common';

export interface GroupFields {
  domain?: ElasticKeyword;
  id?: ElasticKeyword;
  name?: ElasticKeyword;
}
