import { AutonomousSystemFields } from './as';
import { GeoFields } from './geo';
import { ElasticKeyword, IpAddress } from './common';
import { UserFields } from './user';

export interface ServerFields {
  as?: AutonomousSystemFields;
  geo?: GeoFields;
  user?: UserFields;

  address?: ElasticKeyword;
  bytes?: number;
  domain?: ElasticKeyword;
  ip?: IpAddress;
  mac?: ElasticKeyword;
  nat?: {
    ip?: IpAddress;
    port?: number;
  };
  packets?: number;
  port?: number;
  registeredDomain?: ElasticKeyword; // registered_domain
  topLevelDomain?: ElasticKeyword; // top_level_domain
}
