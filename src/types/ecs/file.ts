import { ElasticKeyword } from './common';

import { CodeSignatureFields } from './codeSignature';
import { HashFields } from './hash';
import { PeHeaderFields } from './pe';
import { X509Fields } from './x509';

export type FileAttribute =
  | 'archive'
  | 'compressed'
  | 'directory'
  | 'encrypted'
  | 'execute'
  | 'hidden'
  | 'read'
  | 'readonly'
  | 'system'
  | 'write';

export interface FileFields {
  codeSignature?: CodeSignatureFields;
  hash?: HashFields;
  pe?: PeHeaderFields;
  x509?: X509Fields;

  accessed?: Date;
  attributes?: FileAttribute[];
  created?: Date;
  ctime?: Date;
  device?: ElasticKeyword;
  directory?: ElasticKeyword;
  driveLetter?: ElasticKeyword; // drive_letter
  extension?: ElasticKeyword;
  gid?: ElasticKeyword;
  group?: ElasticKeyword;
  inode?: ElasticKeyword;
  mimeType?: ElasticKeyword; // mime_type
  mode?: ElasticKeyword;
  mtime?: Date;
  name?: ElasticKeyword;
  owner?: ElasticKeyword;
  path?: ElasticKeyword;
  size?: number;
  targetPath?: ElasticKeyword; // target_path
  type?: ElasticKeyword;
  uid?: ElasticKeyword;
}
