import { ElasticKeyword } from './common';

export interface HashFields {
  md5?: ElasticKeyword;
  sha1?: ElasticKeyword;
  sha256?: ElasticKeyword;
  sha512?: ElasticKeyword;
}
