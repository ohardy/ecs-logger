import { ElasticKeyword } from './common';

export interface RegistryFields {
  data?: {
    bytes?: number;
    strings?: ElasticKeyword[];
    type?: ElasticKeyword;
  };
  hive?: ElasticKeyword;
  key?: ElasticKeyword;
  path?: ElasticKeyword;
  value?: ElasticKeyword;
}
