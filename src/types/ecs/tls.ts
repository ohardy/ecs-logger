import { ElasticKeyword } from './common';
import { X509Fields } from './x509';

export interface TlsClientServer {
  certificate?: ElasticKeyword;
  certificateChain?: ElasticKeyword[]; // certificate_chain
  hash?: {
    md5?: ElasticKeyword;
    sha1?: ElasticKeyword;
    sha256?: ElasticKeyword;
  };
  notAfter?: Date; // not_after
  notBefore?: Date; // not_before
  subject?: ElasticKeyword;
  x509?: X509Fields;
}

export interface TlsFields {
  cither?: ElasticKeyword;
  client?: TlsClientServer & {
    ja3?: ElasticKeyword;
    notAfter?: Date; // not_after
    notBefore?: Date; // not_before
    serverName?: ElasticKeyword;
    subject?: ElasticKeyword;
    supportedCiphers?: ElasticKeyword[]; // supported_ciphers
  };
  curve?: ElasticKeyword;
  established?: boolean;
  nextProtocol?: ElasticKeyword; // next_protocol
  resumed?: boolean;
  server?: TlsClientServer & {
    issuer?: ElasticKeyword;
    ja3s?: ElasticKeyword;
    supportedCiphers?: ElasticKeyword[]; // supported_ciphers
  };
  version?: ElasticKeyword;
  versionProtocol?: ElasticKeyword; // version_protocol
}
