import { ElasticKeyword } from './common';
import { CodeSignatureFields } from './codeSignature';
import { HashFields } from './hash';
import { PeHeaderFields } from './pe';

export interface DllFields {
  codeSignature?: CodeSignatureFields;
  hash?: HashFields;
  pe?: PeHeaderFields;

  name?: ElasticKeyword;
  path?: ElasticKeyword;
}
