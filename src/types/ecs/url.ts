import { ElasticKeyword } from './common';

export interface URLFields {
  domain?: ElasticKeyword;
  extension?: ElasticKeyword;
  fragment?: ElasticKeyword;
  full?: ElasticKeyword;
  original?: ElasticKeyword;
  password?: ElasticKeyword;
  path?: ElasticKeyword;
  port?: number;
  query?: ElasticKeyword;
  registeredDomain?: ElasticKeyword; // registered_domain
  scheme?: ElasticKeyword;
  topLevelDomain?: ElasticKeyword; // top_level_domain
  username?: ElasticKeyword;
}
