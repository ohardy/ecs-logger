import { ElasticGeoPoint, ElasticKeyword } from './common';

export interface GeoFields {
  cityName?: ElasticKeyword;
  continentName?: ElasticKeyword; // continent_name
  countryIsoCode?: ElasticKeyword; // country_iso_code
  countryName?: ElasticKeyword; // country_name
  location?: ElasticGeoPoint;
  name?: ElasticKeyword;
  regionIsoCode?: ElasticKeyword; // region_iso_code
  regionName?: ElasticKeyword; // region_name
}
