import { ElasticKeyword, ElasticText, Labels } from './common';

export interface ErrorFields {
  code?: ElasticKeyword;
  id?: ElasticKeyword;
  message?: ElasticText;
  stackTrace?: string; // stack_trace
  'stackTrace.text'?: ElasticText;
  properties?: Labels; // TODO: not in ecs 1.6.0
  type?: ElasticKeyword;
}
