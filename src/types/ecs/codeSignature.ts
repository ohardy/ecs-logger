import { ElasticKeyword } from './common';

export interface CodeSignatureFields {
  exists?: boolean;
  status?: ElasticKeyword;
  subjectName?: ElasticKeyword; // subject_name
  trusted?: boolean;
  valid?: boolean;
}
