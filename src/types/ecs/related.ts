import { ElasticKeyword, IpAddress } from './common';

export interface RelatedFields {
  hash?: ElasticKeyword[];
  hosts?: ElasticKeyword[];
  ip?: IpAddress[];
  user?: ElasticKeyword[];
}
