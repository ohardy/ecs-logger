import { ElasticKeyword } from './common';

/**
 * Distributed tracing makes it possible to analyze performance throughout a
 * microservice architecture all in one view.
 * This is accomplished by tracing all of the requests - from the initial
 * web request in the front-end service - to queries made through multiple
 * back-end services.
 */
export interface TracingFields {
  span?: {
    /**
     * Unique identifier of the span within the scope of its trace.
     *
     * A span represents an operation within a transaction, such as a request
     * to another service, or a database query.
     *
     * type: keyword
     *
     * level: extended
     *
     * @example 3ff9a8981b7ccd5a
     */
    id: ElasticKeyword;
  };

  trace?: {
    /**
     * Unique identifier of the trace.
     *
     * A trace groups multiple events like transactions that belong together.
     * For example, a user request handled by multiple inter-connected services.
     *
     * type: keyword
     *
     * level: extended
     *
     * @example 4bf92f3577b34da6a3ce929d0e0e4736
     */
    id: ElasticKeyword;
  };

  transaction?: {
    /**
     * Unique identifier of the transaction.
     *
     * A transaction is the highest level of work measured within a service,
     * such as a request to a server.
     *
     * type: keyword
     *
     * level: extended
     *
     * @example: 00f067aa0ba902b7
     */
    id: ElasticKeyword;
  };
}
