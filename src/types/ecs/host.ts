import { GeoFields } from './geo';
import { ElasticKeyword, IpAddress } from './common';
import { OperatingSystemFields } from './os';
import { UserFields } from './user';

export interface HostFields {
  geo?: GeoFields;
  os?: OperatingSystemFields;
  user?: UserFields;

  architecture?: ElasticKeyword;
  domain?: ElasticKeyword;
  hostname?: ElasticKeyword;
  ip?: IpAddress[];
  mac?: ElasticKeyword[];
  name?: ElasticKeyword;
  type?: ElasticKeyword;
  uptime?: number;
}
