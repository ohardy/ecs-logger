import { ElasticKeyword } from './common';

export interface LogFields {
  file?: {
    path?: ElasticKeyword;
  };
  level?: ElasticKeyword;
  logger?: ElasticKeyword;
  origin?: {
    file?: {
      line?: number;
      name?: ElasticKeyword;
    };
    function?: ElasticKeyword;
  };
  original?: ElasticKeyword;
  syslog?: {
    facility?: {
      code?: number;
      name?: ElasticKeyword;
    };
    priority?: number;
    severity?: {
      code?: number;
      name?: ElasticKeyword;
    };
  };
}
