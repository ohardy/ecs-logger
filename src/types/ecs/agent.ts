import { ElasticKeyword } from './common';

/* eslint-disable max-len */

/**
 * The agent fields contain the data about the software entity, if any,
 * that collects, detects, or observes events on a host,
 * or takes measurements on a host.
 *
 * Examples include Beats. Agents may also run on observers.
 * ECS agent.* fields shall be populated with details of the agent running
 * on the host or observer where the event happened or the
 * measurement was taken.
 */
export interface AgentFields {
  build?: {
    /**
     * Extended build information for the agent.
     *
     * This field is intended to contain any build information that a data
     * source may provide, no specific formatting is required.
     *
     * type: keyword
     *
     * level: core
     *
     * @example
     * metricbeat version 7.6.0 (amd64), libbeat 7.6.0[6a23e8f8f30f5001ba344e4e54d8d9cb82cb107c built 2020-02-05 23:10:10 +0000 UT
     */
    original: ElasticKeyword;
  };

  /**
   * Ephemeral identifier of this agent (if one exists).
   * This id normally changes across restarts, but agent.id does not.
   *
   * type: keyword
   *
   * level: extended
   *
   * @example 8a4f500f
   */
  ephemeralId?: ElasticKeyword; // ephemeral_id

  /**
   * Unique identifier of this agent (if one exists).
   *
   * Example: For Beats this would be beat.id.
   *
   * type: keyword
   *
   * level: core
   *
   * @example 8a4f500d
   */
  id?: ElasticKeyword;

  /**
   * Custom name of the agent.
   *
   * This is a name that can be given to an agent. This can be helpful if for
   * example two Filebeat instances are running on the same host but a human
   * readable separation is needed on which Filebeat instance data
   * is coming from.
   *
   * If no name is given, the name is often left empty.
   *
   * type: keyword
   *
   * level: core
   *
   * @example foo
   */
  name?: ElasticKeyword;

  /**
   * Type of the agent.
   *
   * The agent type always stays the same and should be given by the agent
   * used.
   * In case of Filebeat the agent would always be Filebeat also if two
   * Filebeat instances are run on the same machine.
   *
   * type: keyword
   *
   * level: core
   *
   * @example filebeat
   */
  type?: ElasticKeyword;

  /**
   * Version of the agent.
   *
   * type: keyword
   *
   * level: core
   *
   * @example 6.0.0-rc2
   */
  version?: ElasticKeyword;
}
