import { IpAddress, ElasticKeyword } from './common';

import { AutonomousSystemFields } from './as';
import { GeoFields } from './geo';
import { UserFields } from './user';

/**
 * A client is defined as the initiator of a network connection for events
 * regarding sessions, connections, or bidirectional flow records.
 *
 * For TCP events, the client is the initiator of the TCP connection that
 * sends the SYN packet(s). For other protocols, the client is generally
 * the initiator or requestor in the network transaction.
 * Some systems use the term "originator" to refer the client in
 * TCP connections.
 * The client fields describe details about the system acting as the client in
 * the network event. Client fields are usually populated in conjunction
 * with server fields. Client fields are generally not populated for
 * packet-level events.
 *
 * Client / server representations can add semantic context to an exchange,
 * which is helpful to visualize the data in certain situations.
 * If your context falls in that category, you should still ensure
 * that source and destination are filled appropriately.
 */
export interface ClientFields {
  as?: AutonomousSystemFields;
  geo?: GeoFields;
  user?: UserFields;

  /**
   * Some event client addresses are defined ambiguously.
   * The event will sometimes list an IP, a domain or a unix socket.
   * You should always store the raw address in the .address field.
   *
   * Then it should be duplicated to .ip or .domain, depending on which
   * one it is.
   *
   * type: keyword
   *
   * level: extended
   */
  address?: ElasticKeyword;

  /**
   * Bytes sent from the client to the server.
   *
   * type: long
   *
   * level: core
   *
   * @example 184
   */
  bytes?: number;

  /**
   * Client domain.
   *
   * type: keyword
   *
   * level: core
   */
  domain?: ElasticKeyword;

  /**
   * IP address of the client (IPv4 or IPv6).
   *
   * type: ip
   *
   * level: core
   */
  ip?: IpAddress;

  /**
   * MAC address of the client.
   *
   * type: keyword
   *
   * level: core
   */
  mac?: ElasticKeyword;

  nat?: {
    /**
     * Translated IP of source based NAT sessions (e.g. internal client to internet).
     *
     * Typically connections traversing load balancers, firewalls, or routers.
     *
     * type: ip
     *
     * level: extended
     */
    ip?: IpAddress;

    /**
     * Translated port of source based NAT sessions (e.g. internal client to internet).
     *
     * Typically connections traversing load balancers, firewalls, or routers.
     *
     * type: long
     *
     * level: extended
     */
    port?: number;
  };

  /**
   * Packets sent from the client to the server.
   *
   * type: long
   *
   * level: core
   *
   * @example 12
   */
  packets?: number;

  /**
   * Port of the client.
   *
   * type: long
   *
   * level: core
   */
  port?: number;

  /**
   * The highest registered client domain, stripped of the subdomain.
   *
   * For example, the registered domain for "foo.google.com" is "google.com".
   *
   * This value can be determined precisely with a list like the public
   * suffix list (http://publicsuffix.org). Trying to approximate this by
   * simply taking the last two labels will not work well for TLDs such as "co.uk".
   *
   * type: keyword
   *
   * level: extended
   *
   * @example google.com
   */
  registeredDomain?: ElasticKeyword; // registered_domain

  /**
   * The effective top level domain (eTLD), also known as the domain suffix,
   * is the last part of the domain name. For example, the top level
   * domain for google.com is "com".
   *
   * This value can be determined precisely with a list like the public suffix
   * list (http://publicsuffix.org). Trying to approximate this by simply
   * taking the last label will not work well for effective TLDs such as "co.uk".
   *
   * type: keyword
   *
   * @example co.uk
   */
  topLevelDomain?: ElasticKeyword; // top_level_domain
}
