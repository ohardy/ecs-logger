export type ElasticGeoPoint = {
  long: number;
  lat: number;
};
export type ElasticKeyword = string;
export type ElasticText = string;
export type IpAddress = string;
export type Labels = { [string: string]: string };
export type Tags = string[];
export type DateIsoString = string;
