import { HashFields } from './hash';
import { PeHeaderFields } from './pe';
import { ElasticKeyword, ElasticText } from './common';
import { CodeSignatureFields } from './codeSignature';

export interface ProcessFieldsBase {
  codeSignature?: CodeSignatureFields;
  hash?: HashFields;

  args?: ElasticKeyword[];
  argsCount?: number; // args_count
  commandLine?: ElasticKeyword; // command_line
  entityId?: ElasticKeyword; // entity_id
  executable?: ElasticKeyword;
  exitCode?: number; // exit_code
  name?: ElasticKeyword;
  pgid?: number;
  pid?: number;
  ppid?: number;
  start?: Date;
  thread?: {
    id?: number;
    name?: ElasticKeyword;
  };
  title?: ElasticKeyword;
  uptime?: number;
  workingDirectory?: ElasticKeyword; // working_directory
  'workingDirectory.text'?: ElasticText; // working_directory
}

export interface ProcessFields extends ProcessFieldsBase {
  pe?: PeHeaderFields;

  parent?: ProcessFieldsBase;
}
