import { ElasticKeyword, IpAddress } from './common';
import { VlanFields } from './vlan';

export type NetworkDirection =
  | 'inbound'
  | 'outbound'
  | 'internal'
  | 'external'
  | 'unknown';

export interface NetworkInner {
  vlan?: VlanFields;
}

export interface NetworkFields {
  application?: ElasticKeyword;
  bytes?: number;
  communityId?: ElasticKeyword; // community_id
  direction?: NetworkDirection;
  forwardedIp?: IpAddress; // forwarded_ip
  ianaNumber?: number; // iana_number
  inner?: NetworkInner;
  name?: ElasticKeyword;
  packets?: number;
  protocol?: ElasticKeyword;
  transport?: ElasticKeyword;
  type?: ElasticKeyword;
  vlan?: VlanFields;
}
