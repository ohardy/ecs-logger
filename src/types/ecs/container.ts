import { ElasticKeyword, Labels } from './common';

export interface ContainerFields {
  id?: ElasticKeyword;
  image?: {
    name?: ElasticKeyword;
    tag?: ElasticKeyword;
  };
  labels?: Labels;
  name?: ElasticKeyword;
  runtime?: ElasticKeyword;
}
