import { ElasticKeyword } from './common';

export interface InterfaceFields {
  alias?: ElasticKeyword;
  id?: ElasticKeyword;
  name?: ElasticKeyword;
}
