import { ElasticKeyword } from './common';

export type EventCategory =
  | 'authentication'
  | 'database'
  | 'driver'
  | 'file'
  | 'host'
  | 'iam'
  | 'intrusion_detection'
  | 'malware'
  | 'network'
  | 'package'
  | 'process'
  | 'web';

export type EventKind =
  | 'alert'
  | 'event'
  | 'metric'
  | 'state'
  | 'pipeline_error'
  | 'signal';

export type EventOutcome = 'failure' | 'success' | 'unknown';

export type EventType =
  | 'access'
  | 'admin'
  | 'allowed'
  | 'change'
  | 'connection'
  | 'creation'
  | 'deletion'
  | 'denied'
  | 'end'
  | 'error'
  | 'group'
  | 'info'
  | 'installation'
  | 'protocol'
  | 'start'
  | 'user';

export interface EventFields {
  action?: ElasticKeyword;
  category?: EventCategory[];
  code?: ElasticKeyword;
  created?: Date;
  dataset?: ElasticKeyword;
  duration?: number;
  end?: Date;
  hash?: ElasticKeyword;
  id?: ElasticKeyword;
  ingested?: Date;
  kind?: EventKind;
  module?: ElasticKeyword;
  original?: ElasticKeyword;
  outcome?: EventOutcome;
  provider?: ElasticKeyword;
  reason?: ElasticKeyword;
  reference?: ElasticKeyword;
  riskScore?: number; // risk_score
  riskScoreNorm?: number; // risk_score_norm
  sequence?: number;
  severity?: number;
  start?: Date;
  timezone?: ElasticKeyword;
  type?: EventType;
  url?: ElasticKeyword;
}
