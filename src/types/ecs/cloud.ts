import { ElasticKeyword } from './common';

/**
 * Fields related to the cloud or infrastructure the events are coming from.
 */
export interface CloudFields {
  account?: {
    id?: ElasticKeyword;
  };
  availabilityZone?: ElasticKeyword; // availability_zone
  instance?: {
    id?: ElasticKeyword;
    name?: ElasticKeyword;
  };
  machine?: {
    type?: ElasticKeyword;
  };
  provider?: ElasticKeyword;
  region?: ElasticKeyword;
}
