import { ElasticKeyword } from './common';

export interface ThreatFields {
  framework?: ElasticKeyword;
  tactic?: {
    id?: ElasticKeyword;
    name?: ElasticKeyword;
    reference?: ElasticKeyword;
  };
  technique?: {
    id?: ElasticKeyword;
    name?: ElasticKeyword;
    reference?: ElasticKeyword;
  };
}
