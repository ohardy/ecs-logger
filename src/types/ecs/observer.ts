import { GeoFields } from './geo';
import { InterfaceFields } from './interface';
import { ElasticKeyword, IpAddress } from './common';
import { VlanFields } from './vlan';
import { OperatingSystemFields } from './os';

export interface ObserverEgressIngress {
  zone?: ElasticKeyword;
  interface?: InterfaceFields;
  vlan?: VlanFields;
}

export interface ObserverFields {
  geo?: GeoFields;
  os?: OperatingSystemFields;

  egress?: ObserverEgressIngress;
  ingress?: ObserverEgressIngress;

  hostname?: ElasticKeyword;
  ip?: IpAddress;
  mac?: ElasticKeyword;
  name?: ElasticKeyword;
  product?: ElasticKeyword;
  serialNumber?: ElasticKeyword; // serial_number
  type?: ElasticKeyword;
  vendor?: ElasticKeyword;
  version?: ElasticKeyword;
}
