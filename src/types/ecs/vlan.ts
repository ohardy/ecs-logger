import { ElasticKeyword } from './common';

export interface VlanFields {
  id?: ElasticKeyword;
  name?: ElasticKeyword;
}
