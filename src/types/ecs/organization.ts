import { ElasticKeyword, ElasticText } from './common';

export interface OrganizationFields {
  id?: ElasticKeyword;
  name?: ElasticKeyword;
  'name.text'?: ElasticText;
}
