import {
  AgentFields,
  BaseFields,
  ClientFields,
  CloudFields,
  ContainerFields,
  DestinationFields,
  DllFields,
  DNSFields,
  ECSFields,
  EcsLoggerFields,
  ErrorFields,
  EventFields,
  FileFields,
  GeoFields,
  GroupFields,
  HostFields,
  HttpFields,
  LogFields,
  NetworkFields,
  ObserverFields,
  OrganizationFields,
  PackageFields,
  ProcessFields,
  RegistryFields,
  RelatedFields,
  RuleFields,
  ServerFields,
  ServiceFields,
  SourceFields,
  ThreatFields,
  TlsFields,
  TracingFields,
  URLFields,
  UserAgentFields,
  UserFields,
  VulnerabilityFields,
} from './ecs';

/* eslint-disable @typescript-eslint/indent */
export interface EcsDocument
  extends BaseFields,
    EcsLoggerFields,
    TracingFields {
  agent?: AgentFields;
  client?: ClientFields;
  cloud?: CloudFields;
  container?: ContainerFields;
  destination?: DestinationFields;
  dll?: DllFields;
  dns?: DNSFields;
  ecs?: ECSFields;
  error?: ErrorFields;
  event?: EventFields;
  file?: FileFields;
  geo?: GeoFields;
  group?: GroupFields;
  host?: HostFields;
  http?: HttpFields;
  log?: LogFields;
  network?: NetworkFields;
  observer?: ObserverFields;
  organization?: OrganizationFields;
  package?: PackageFields;
  process?: ProcessFields;
  registry?: RegistryFields;
  related?: RelatedFields;
  rule?: RuleFields;
  server?: ServerFields;
  service?: ServiceFields;
  source?: SourceFields;
  threat?: ThreatFields;
  tls?: TlsFields;
  url?: URLFields;
  user?: UserFields;
  userAgent?: UserAgentFields; // user_agent
  vulnerability?: VulnerabilityFields;
}
/* eslint-enable @typescript-eslint/indent */
