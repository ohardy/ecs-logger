import { Optional } from 'utility-types';

import { EcsDocument } from './ecsDocument';
import { EventSeverity } from '../severity';

export * from './ecs';
export * from './ecsDocument';

// export type FinalLogEvent = EcsDocument & {
//   '@timestamp': Date;
//   message: string;
//   // log: RawLogEvent;
// };

/* eslint-disable @typescript-eslint/indent */
export type EcsDocumentWithoutBase = Optional<
  EcsDocument,
  '@timestamp' | 'message'
>;
/* eslint-enable @typescript-eslint/indent */

// export type EcsLog = LogEvent;

export abstract class Renderer {
  constructor(public colorEnabled: boolean) {}

  abstract render(ecsDocument: EcsDocument): string;
}

export type MiddlewareArguments = {
  severity: EventSeverity;
  config: LoggerConfiguration;
  message: string;
  ecsDocument: EcsDocument;
  error: Error | null;
};
export type MiddlewareFunction = (args: MiddlewareArguments) => EcsDocument;

export type LoggerConfiguration = {
  severity: EventSeverity;
  middlewares: MiddlewareFunction[];
  write: (str: string) => boolean;
  ecsDocument: EcsDocumentWithoutBase;
  filename?: string;
  name?: string;
  renderer: Renderer;
};

// export type MiddlewareArguments = {
//   severity: EventSeverity;
//   config: LoggerConfiguration;
//   message: string;
//   ecsDocument: EcsDocumentWithoutBase;
//   error?: Error | null;
// };

export type FormatLineFunction = (
  partName: string,
  value: any | null | undefined,
  ecsDocument: EcsDocument
) => string;

export type FormatFunction = (
  value: any | null | undefined,
  ecsDocument: EcsDocument
) => string | null | undefined;
