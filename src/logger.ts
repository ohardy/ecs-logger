import { merge } from 'lodash';

import { getBaseConfiguration } from './base';
import { EventSeverity } from './severity';
import {
  EcsDocument,
  EcsDocumentWithoutBase,
  LoggerConfiguration,
} from './types';

export class Logger {
  config: LoggerConfiguration;

  parent?: Logger;

  /**
   * ECS version
   */
  static version = '1.6.0';

  /**
   * Creates an instance of Logger.
   */
  constructor(config: Partial<LoggerConfiguration> = {}, parent?: Logger) {
    this.config = merge(getBaseConfiguration(), config);
    this.parent = parent;
  }

  /* eslint-disable class-methods-use-this */
  /**
   * Return current timestamp
   */
  __getCurrentDate(): Date {
    return new Date();
  }
  /* eslint-enable class-methods-use-this */

  /**
   * Return log event
   */
  createLogDocument(
    severity: number,
    message: string,
    ecsDocument: EcsDocumentWithoutBase,
    error: Error | null = null
  ): EcsDocument {
    let newEcsDocument: EcsDocumentWithoutBase | EcsDocument = merge(
      {},
      ecsDocument,
      {
        ecs: {
          version: Logger.version,
        },
        message,
      }
    );

    if (newEcsDocument['@timestamp'] == null) {
      newEcsDocument['@timestamp'] = this.__getCurrentDate();
    }

    for (const middleware of this.config.middlewares) {
      newEcsDocument = middleware({
        severity,
        config: this.config,
        message,
        ecsDocument: newEcsDocument as EcsDocument,
        error,
      });
    }

    return newEcsDocument as EcsDocument;
  }

  getMergedEcsDocument(): EcsDocumentWithoutBase {
    if (this.parent == null) {
      return this.config.ecsDocument;
    }

    return merge(
      {},
      this.parent.getMergedEcsDocument(),
      this.config.ecsDocument
    );
  }

  logRawDocument(ecsDocument: EcsDocument): void {
    const newEcsDocument = merge({}, this.getMergedEcsDocument(), ecsDocument);

    const str = this.config.renderer.render(newEcsDocument);

    this.config.write(str);
  }

  /**
   * Log
   */
  log(
    severity: EventSeverity,
    message: string,
    ecsDocument: EcsDocumentWithoutBase,
    error: Error | null = null
  ) {
    if (severity < this.config.severity) {
      return;
    }

    this.logRawDocument(
      this.createLogDocument(severity, message, ecsDocument, error)
    );
  }

  /**
   * Log a trace
   */
  trace(
    message: string,
    ecsDocument: EcsDocumentWithoutBase = {},
    error?: Error
  ) {
    this.log(EventSeverity.Trace, message, ecsDocument, error);
  }

  /**
   * Log a debug
   */
  debug(
    message: string,
    ecsDocument: EcsDocumentWithoutBase = {},
    error?: Error
  ) {
    this.log(EventSeverity.Debug, message, ecsDocument, error);
  }

  /**
   * Log an info
   */
  info(
    message: string,
    ecsDocument: EcsDocumentWithoutBase = {},
    error?: Error
  ) {
    this.log(EventSeverity.Info, message, ecsDocument, error);
  }

  /**
   * Log a notice
   */
  notice(
    message: string,
    ecsDocument: EcsDocumentWithoutBase = {},
    error?: Error
  ) {
    this.log(EventSeverity.Notice, message, ecsDocument, error);
  }

  /**
   * Log a warning
   */
  warn(
    message: string,
    ecsDocument: EcsDocumentWithoutBase = {},
    error?: Error
  ) {
    this.log(EventSeverity.Warning, message, ecsDocument, error);
  }

  /**
   * Log an error
   */
  error(
    message: string,
    error?: Error | null,
    ecsDocument: EcsDocumentWithoutBase = {}
  ) {
    this.log(EventSeverity.Error, message, ecsDocument, error);
  }

  /**
   * Log a critical
   */
  critical(
    message: string,
    error: Error | null,
    ecsDocument: EcsDocumentWithoutBase = {}
  ) {
    this.log(EventSeverity.Critical, message, ecsDocument, error);
  }

  /**
   * Log an alert
   */
  alert(
    message: string,
    error: Error | null,
    ecsDocument: EcsDocumentWithoutBase = {}
  ) {
    this.log(EventSeverity.Alert, message, ecsDocument, error);
  }

  /**
   * Log an emergency
   */
  emergency(
    message: string,
    error: Error | null,
    ecsDocument: EcsDocumentWithoutBase = {}
  ) {
    this.log(EventSeverity.Emergency, message, ecsDocument, error);
  }

  /**
   * Create new logger
   */
  static createLogger(
    config: Partial<LoggerConfiguration> = {},
    parent?: Logger
  ): Logger {
    return new Logger(config, parent);
  }

  /**
   * Create sub logger
   */
  createSubLogger(config: Partial<LoggerConfiguration> = {}): Logger {
    return Logger.createLogger(merge({}, this.config, config), this);
  }
}

export const logger = Logger.createLogger();

export const { createLogger } = Logger;
