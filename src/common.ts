import { toSafeInteger } from 'lodash';

import { EVENT_DURATIONS } from './constants';

/**
 * Colorize duration
 */
export function colorizeDuration(durationInNs: number): string {
  const durationInMs = durationInNs / 1000000;
  let colorize = (str: string): string => str;
  for (const duration of EVENT_DURATIONS) {
    if (duration.minDuration < durationInMs) {
      colorize = duration.colorize;
      break;
    }
  }

  if (durationInMs > 1000) {
    return colorize(` ${toSafeInteger(durationInMs / 1000).toFixed(0)}s `);
  }

  return colorize(` ${toSafeInteger(durationInMs).toFixed(0)}ms `);
}
