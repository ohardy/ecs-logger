import { toString } from 'lodash';

import { Logger } from './logger';

export function handleUnhandledRejection(logger: Logger) {
  process.on('unhandledRejection', (error) => {
    if (error instanceof Error) {
      logger.error('Channel error', error);
    } else {
      logger.error(toString(error));
    }
  });
}
