import {
  errorMiddleware,
  eventMiddleware,
  hostMiddleware,
  processMiddleware,
  serviceMiddleware,
} from './middlewares';
import { LoggerConfiguration } from './types';
import { EventSeverity, getEventSeverityFromName } from './severity';

import JsonRenderer from './renderers/JsonRenderer';
import PrettyRenderer from './renderers/PrettyRenderer';

type Renderers = { [key: string]: typeof JsonRenderer | typeof PrettyRenderer };

const RENDERERS: Renderers = {
  JsonRenderer,
  PrettyRenderer,
};

type AllRenderer =
  | typeof JsonRenderer
  | typeof PrettyRenderer
  | null
  | undefined;

/**
 * Return Renderer instance from env
 */
export function getRendererFromEnv(
  rendererClassName: string | undefined,
  DefaultRendererClassName: typeof JsonRenderer | typeof PrettyRenderer
): JsonRenderer | PrettyRenderer {
  let RendererClass: AllRenderer;

  if (typeof rendererClassName === 'string') {
    RendererClass = RENDERERS?.[rendererClassName];
  }

  if (RendererClass == null) {
    RendererClass = DefaultRendererClassName;
  }

  return new RendererClass();
}

let baseConfiguration: LoggerConfiguration | undefined;

/**
 * Return base configuration
 */
export function getBaseConfiguration(): LoggerConfiguration {
  if (baseConfiguration == null) {
    baseConfiguration = {
      severity: getEventSeverityFromName(
        process.env.ECS_EVENT_SEVERITY,
        EventSeverity.Info
      ),
      middlewares: [
        errorMiddleware,
        eventMiddleware,
        hostMiddleware,
        processMiddleware,
        serviceMiddleware,
      ],
      ecsDocument: {},
      renderer: getRendererFromEnv(
        process.env.ECS_LOGGER_RENDERER,
        PrettyRenderer
      ),
      write(str: string): boolean {
        return process.stdout.write(`${str}\n`);
      },
    };
  }

  return baseConfiguration;
}
