import { get } from 'lodash';

import { EcsDocument, FormatLineFunction, Renderer } from '../../types';
import formatWithColors from './formatWithColors';
import { LINE_ELEMENTS } from '../../constants';

export function formatLine(
  ecsDocument: EcsDocument,
  doFormatPart: FormatLineFunction
): string {
  const parts = [];
  for (const part of LINE_ELEMENTS) {
    const value = get(ecsDocument, part);
    if (value != null) {
      parts.push(doFormatPart(part, value, ecsDocument));
    }
  }

  return parts.join(' ');
}

/**
 * PrettyRenderer
 */
export default class PrettyRenderer extends Renderer {
  constructor() {
    super(true);
  }

  render(ecsDocument: EcsDocument): string {
    return formatLine(ecsDocument, formatWithColors);
  }
}
