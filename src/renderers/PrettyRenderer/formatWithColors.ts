import { inspect } from 'util';

import { isEmpty, pad } from 'lodash';
import { format } from 'date-fns';

import {
  EVENT_MODULE_COLOR,
  EVENT_SEVERITY_COLORS,
  EVENT_SEVERITY_NAMES,
} from '../../constants';
import { colorizeDuration } from '../../common';
import { EventSeverity } from '../../severity';
import {
  ElasticText,
  EcsDocument,
  FormatFunction,
  ErrorFields,
} from '../../types';

const FORMATS: { [key: string]: FormatFunction } = {
  '@timestamp': (value: Date | null | undefined): string | null => {
    if (value == null) {
      return null;
    }

    return format(value, 'HH:mm:ss');
  },
  'event.duration': (
    value: number | null | undefined
  ): string | null | undefined => {
    if (value != null) {
      return colorizeDuration(value);
    }
    return null;
  },
  'event.module': (value: string | null | undefined): string | null => {
    if (value != null) {
      return EVENT_MODULE_COLOR(` ${value.toUpperCase()} `);
    }
    return '';
  },
  error: (error: ErrorFields | null): string | null => {
    const parts = [];

    if (error == null) {
      return null;
    }

    const { properties, stackTrace } = error;
    if (stackTrace != null) {
      parts.push(
        stackTrace
          .split('\n')
          // .map((str: string) => {
          //   return trim(str);
          // })
          .join('\n')
      );
    }

    if (properties != null && !isEmpty(properties)) {
      parts.push(
        inspect(properties, {
          colors: true,
          compact: false,
        })
      );
    }

    if (parts.length === 0) {
      return null;
    }

    return `\n${parts.join('\n')}`;
  },
  'event.severity': (value: EventSeverity | null | undefined): any => {
    if (value == null) {
      return null;
    }

    let severity: string = EVENT_SEVERITY_NAMES[value];
    const colorizeFunc: Function = EVENT_SEVERITY_COLORS[value];
    const severityLength = severity.length;
    severity = ` ${severity}${pad(' ', Math.max(0, 6 - severityLength))}`;

    return colorizeFunc(severity);
  },
  message: (
    value: ElasticText | null | undefined,
    ecsDocument: EcsDocument
  ) => {
    const { coloredMessage } = ecsDocument;

    if (coloredMessage != null) {
      return coloredMessage;
    }

    return value;
  },
};

export default function formatWithColors(
  partName: string,
  value: any | null | undefined,
  ecsDocument: EcsDocument
): string {
  const func = FORMATS[partName];

  let newValue = value;

  if (func != null) {
    newValue = func(value, ecsDocument);
  }

  if (newValue == null) {
    return '';
  }

  return newValue;
}
