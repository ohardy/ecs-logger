import { snakeCase } from 'lodash';
import { EcsDocument, Renderer } from '../types';

export function replacer(key: string, value: any) {
  if (['coloredMessage', 'originalError'].includes(key)) {
    return undefined;
  }

  if (value != null) {
    if (Array.isArray(value)) {
      return value;
    }

    if (typeof value === 'object') {
      const replacement: { [key: string]: any } = {};
      /* eslint-disable array-callback-return */
      Object.keys(value).map((subKey: string): void => {
        replacement[snakeCase(subKey)] = value[subKey];
      });
      return replacement;
    }
  }

  return value;
}

/**
 * JsonRenderer
 */
export default class JsonRenderer extends Renderer {
  constructor() {
    super(false);
  }

  render(ecsDocument: EcsDocument): string {
    return JSON.stringify(ecsDocument, replacer);
  }
}
