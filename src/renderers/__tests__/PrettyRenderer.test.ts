import PrettyRenderer from '../PrettyRenderer';

describe('PrettyRenderer', (): void => {
  it('should exist', (): void => {
    expect(typeof PrettyRenderer).toEqual('function');
  });
});
