import JsonRenderer, { replacer } from '../JsonRenderer';

const REPLACER_TESTS = [
  {
    toTest: {
      keyInCamelCase: {
        anotherKey: 'A test here',
      },
    },
    result: {
      key_in_camel_case: { anotherKey: 'A test here' },
    },
  },
  {
    toTest: {
      keyInCamelCase: {
        anotherKey: ['a-label', 'anotherLabel'],
      },
    },
    result: {
      key_in_camel_case: { anotherKey: ['a-label', 'anotherLabel'] },
    },
  },
  {
    toTest: ['a-label', 'anotherLabel'],
    result: ['a-label', 'anotherLabel'],
  },
];

describe('JsonRenderer', (): void => {
  describe('JsonRenderer', (): void => {
    it('should exist', (): void => {
      expect(typeof JsonRenderer).toEqual('function');
    });
  });

  describe('replacer', (): void => {
    it('should exist', (): void => {
      expect(typeof replacer).toEqual('function');
    });

    REPLACER_TESTS.forEach((test, index) => {
      it(`should transform keys to snake case #${index}`, (): void => {
        const result = replacer('', test.toTest);

        expect(result).toEqual(test.result);
      });
    });
  });
});
