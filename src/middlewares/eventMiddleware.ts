import { merge } from 'lodash';

import { EventFields, MiddlewareArguments } from '../types';

export default function logMiddleware({
  severity,
  ecsDocument,
}: MiddlewareArguments) {
  const event: EventFields = {
    severity,
  };

  return merge({}, ecsDocument, {
    event,
  });
}
