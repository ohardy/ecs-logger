import errorMiddleware from '../errorMiddleware';

describe('errorMiddleware', (): void => {
  it('should exist', (): void => {
    expect(typeof errorMiddleware).toEqual('function');
  });
});
