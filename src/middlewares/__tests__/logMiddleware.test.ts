import eventMiddleware from '../eventMiddleware';

describe('eventMiddleware', (): void => {
  it('should exist', (): void => {
    expect(typeof eventMiddleware).toEqual('function');
  });
});
