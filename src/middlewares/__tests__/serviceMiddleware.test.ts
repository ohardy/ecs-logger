import serviceMiddleware from '../serviceMiddleware';

describe('serviceMiddleware', (): void => {
  it('should exist', (): void => {
    expect(typeof serviceMiddleware).toEqual('function');
  });
});
