import processMiddleware from '../processMiddleware';

describe('processMiddleware', (): void => {
  it('should exist', (): void => {
    expect(typeof processMiddleware).toEqual('function');
  });
});
