import hostMiddleware from '../hostMiddleware';

describe('hostMiddleware', (): void => {
  it('should exist', (): void => {
    expect(typeof hostMiddleware).toEqual('function');
  });
});
