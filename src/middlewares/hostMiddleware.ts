import os from 'os';

import { merge } from 'lodash';

import { HostFields, MiddlewareArguments } from '../types';

let hostFields: HostFields | undefined;

export default function hostMiddleware({ ecsDocument }: MiddlewareArguments) {
  if (hostFields == null) {
    hostFields = {
      architecture: process.arch,
      name: os.hostname(),
      os: {
        platform: os.platform(),
        version: os.release(),
      },
    };
  }

  return merge({}, ecsDocument, {
    host: hostFields,
  });
}
