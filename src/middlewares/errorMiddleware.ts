import { merge, toString } from 'lodash'; // toString, trim
import { Info, VError } from 'verror';

import { MiddlewareArguments, ErrorFields } from '../types'; // EcsLoggerFields

declare global {
  interface Error {
    code?: number | string;
  }
}

export type StringInfo = { [key: string]: string };

export function infoToStringDict(info: Info): StringInfo {
  return Object.entries(info).reduce((previousValue, currentValue) => {
    const [key, value] = currentValue;

    return {
      ...previousValue,
      [key]: JSON.stringify(value),
    };
  }, {});
}

export default function errorMiddleware({
  error,
  ecsDocument,
}: MiddlewareArguments) {
  if (error == null) {
    return ecsDocument;
  }

  const properties: StringInfo = infoToStringDict(VError.info(error));

  // @ts-ignore
  if (error.errors != null && typeof error.errors !== 'function') {
    // @ts-ignore
    properties.errors = JSON.stringify(error.errors);
  }

  const errorFields: ErrorFields = {
    code: toString(error.code),
    id: error.name,
    message: error.message,
    properties,
    type: error.name,
    stackTrace: VError.fullStack(error),
  };

  return merge({}, ecsDocument, {
    error: errorFields,
  });
}
