import { merge } from 'lodash';

import { MiddlewareArguments, ServiceFields } from '../types';

let serviceFields: ServiceFields | undefined;

export default function serviceMiddleware({
  ecsDocument,
}: MiddlewareArguments) {
  if (serviceFields == null) {
    serviceFields = {
      // runtime: {
      //   name: 'node',
      //   version: process.versions.node,
      // },
      // language: {
      //   name: 'javascript',
      // },
    };
  }

  return merge({}, ecsDocument, {
    service: serviceFields,
  });
}
