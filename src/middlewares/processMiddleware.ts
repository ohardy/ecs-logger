import { merge } from 'lodash';

import { MiddlewareArguments, ProcessFields } from '../types';

let processFields: ProcessFields | undefined;

export default function processMiddleware({
  ecsDocument,
}: MiddlewareArguments) {
  if (processFields == null) {
    processFields = {
      executable: process.execPath,
      args: process.argv,
      pid: process.pid,
      title: process.title,
      workingDirectory: process.cwd(),
      ppid: process.ppid,
    };
  }

  return merge({}, ecsDocument, {
    process: processFields,
  });
}
